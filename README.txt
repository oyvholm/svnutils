README for the svnutils project

Project web page     : http://svnutils.tigris.org
Subversion repository: http://svn.sunbase.org/repos/svnutils

WHAT IS THIS?

  A collection of scripts, libraries and tips for use with the 
  Subversion version control system — <http://subversion.tigris.org>.

  There are some tasks that are repeated several times a day, things 
  like examining svn logs and possibly change the wording, checking for 
  updates in specific ways, synchronising repositories, etc… Many of us 
  end up creating small scripts or aliases for easening those tedious 
  tasks and they end up living in ~/bin/ without anyone else getting the 
  use of it.

  This project is dedicated to those helpful little scripts or possibly 
  larger programs designed for complex and heavy Subversion jobs. No 
  special rules about what can be included here, except for two things:

    1. It must be Free Software. No proprietary or closed software can 
       be included. It must qualify to the guidelines defined by the 
       Free Software Foundation (URL coming later) and preferably be 
       GPL-compatible.
    2. To avoid forking and confusion about where to get the newest 
       version of a program, it should not have an official download 
       page on the Internet. An exception to this is if the author says 
       it OK to import the parts or everything of it into the project.

  A list of the included programs and scripts can be found in the 
  CONTENTS file.

DIRECTORY STRUCTURE

  If you checked this source out from Subversion, this is the layout of 
  the repository:

    - /trunk/
      This is where the development happens. Download this if you want 
      the newest versions of everything. A complete list of the trunk 
      contents can be found in /trunk/CONTENTS .
      - lib/
        Libraries for various programming languages
        - perl
          Perl modules and libraries.
      - src/
        Newest version of the sources.
      - unfinished/
        Yes, unfinished scripts.
    - /branches/
      Various branches of the projects.
    - /tags/
      Snapshots of possible releases and such.
    - /www/
      HTML files on http://svnutils.tigris.org

CONTRIBUTING TO THE PROJECT

  First of all, ensure that all parts of the program is free so it can 
  be incorporated into the project. We’re not going to make any fuzz of 
  it, but when choosing a license for the program, please try to keep it 
  GPL-compatible to make it possible to mix and borrow source parts into 
  other utilities in the svnutils project.

  The preferred charset in text and source files is UTF-8. The use of 
  Unicode and extremely non-English characters is widespread, so the use 
  of a uniform character set saves us for the mess with multiple charset 
  encodings. Please make sure that you programs deals correctly with 
  UTF-8 streams, as this makes communication with the Subversion system 
  much easier.

$Id$
vim: set tw=72 ts=2 sw=2 sts=2 et fo+=wn fo-=2 fenc=UTF-8 :
