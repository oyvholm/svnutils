#!/bin/sh

#
# This script runs through all files in a hierarchy and sets the
# Subversion MIME type on all recognized file extensions.
# It takes one or more "mime.types" files as input, which are in
# the format of Apache's "mime.types" file.
#

scan_types()
{
    grep -v ^# "$1" \
      | awk '{ for (i=2; i<=NF; i++) printf "%s %s\n", $1, $i }' \
      | while read TYPE SUFFIX; do
	if [ "${SUFFIX}" = "$3" ]; then
	    if [ "${QUIET}" != "true" ]; then
		echo svn propset -q svn:mime-type "${TYPE}" "$2"
	    fi
	    if [ "${DRYRUN}" != "true" ]; then
		svn propset -q svn:mime-type "${TYPE}" "$2"
	    fi
	    return
	fi
    done
}

usage()
{
    echo "Usage: set-mime [-n] [-q] directory mime.types [ mime.types ... ]"
    printf "\t-n\tDon't actually do anything, just pretend\n"
    printf "\t-q\tQuiet mode: don't display svn commands\n"
    exit 1
}

# Parse command line
while :
    do case "$1" in
	-q)
	    QUIET="true"
	    shift
	    ;;
	-n)
	    DRYRUN="true"
	    shift
	    ;;
	--)
	    shift
	    break
	    ;;
	-*)
	    usage
	    ;;
	*)
	    break
	    ;;
    esac
done
case $# in
    [01])
	usage
	;;
    *)
	DIR="$1"
	shift
	;;
esac
i=$#
while [ "$i" -gt 0 ]; do
    MIMES="${MIMES} $1"
    shift
    i=`expr $i - 1`
done

find ${DIR} \( \! -name '.svn' -o -prune \) -a -type f -print \
  | while read FILE; do
    SUFFIX=`expr "$FILE" : ".*\.\([^.]*\)$"`
    if [ "$SUFFIX" != "" ]; then
	for MIME in ${MIMES}; do
	    scan_types $MIME $FILE $SUFFIX 
	done
    fi
done
