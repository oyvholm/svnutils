GENERAL INFO
This is a collection of scripts which runs primarly on the windwos platform. Some
may be used standalone, other scripts are used together in as an example post-commit.
For usage instructions run the script with /? option or double click each wsf file.

Settings -
Some of the -wsf files has some settings you can tweak. They are found marked as 
//** Config Section *****************
Which you can edit to your likings.

FILES

SvnHotCopy.wsf
Made to run as scheduled task from a directory you are free to choose. 
If you like to use it in a post-commit, use the files post-commit.bat and 
post-commit-wrapper.wsf together with SvnHotCopy.wsf and place them all in the
hooks directory.

post-commit.bat
To be place in the repository hooks directory. This file is made to call the script
post-commit-wrapper.wsf.

post-commit-wrapper.wsf
A wrapper script that calls other scripts. In this version calls the SvnHotCopy.wsf
