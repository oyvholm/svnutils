List of contents for the svnutils project — http://svnutils.tigris.org

- COMMITTERS
    List of users with commit access to the svnutils repository.
- COPYING
    The GNU General Public License.
- README.txt
    General info about the project
- lib/
  - perl/
    Directory for Perl modules and libraries.
- src/
  - asvn
    Archive SVN (asvn) will allow the recording of file types not 
    normally handled by svn. Currently this includes devices, symlinks 
    and file ownership/permissions.
  - convkeyw
    Strips the dollars and keyword name directly from text files. 
    Designed for use when releasing files and the keywords should not be 
    changed by other programs.
  - findrev
    Searches for a specific revision by running a test command or 
    script. Start and end revision can be specified, and the program 
    uses binary search to locate the revision. Can be fully automated.
  - mergesvn
    Merge changes between files or directories and store the point of 
    the last merge in a special "mergesvn" property. A master location 
    (file or directory) is defined, and other elements in the same or 
    another repository can be updated along with the development in the 
    master element.
  - nosvn
    The tiniest script of them all, but also quite useful in pipes and 
    shell commands. Filters out .svn directories from a stream of file 
    names.
  - svedit
    Shortcut for editing log messages. Can be run directly in the 
    working copy or against a remote or local repository.
  - svnbranchroot
    Find the root of a branch. Takes one parameter — the branch, and 
    finds the root revision of the branch — the revision from which it 
    was made. Especially good for investigating tags. What revision of 
    the parent branch was this tag made from?
  - sident
    A better (IMHO) ident(1). Does not abort on directories or other 
    non-files, limits the output to known keywords by CVS and 
    Subversion, and also lists compressed keywords.
  - svnclean
    Quick and easy way to clean up your working copy. Resets the working 
    copy to a fresh state, like it has been recently checked out and not 
    modified.
  - svndiff
    Script for looking at differences in a directory tree. Uses 
    vimdiff(1) as default, but can also use other diff programs for 
    viewing.
  - svnmimeset.sh
    This script runs through all files in a hierarchy and sets the 
    Subversion MIME type on all recognized file extensions. It takes one 
    or more "mime.types" files as input, which are in the format of 
    Apache's "mime.types" file.
  - svnrevs
    One-liner when called instead of the svn executable will display all 
    revisions as a comma-separated list. Can be used for log messages, 
    scripts etc. Needs Perl.
  - windows/
    - SvnHotCopy.wsf
      Made to run as scheduled task from a directory you are free to 
      choose. If you like to use it in a post-commit, use the files 
      post-commit.bat and post-commit-wrapper.wsf together with 
      SvnHotCopy.wsf and place them all in the hooks directory.
    - post-commit-wrapper.wsf
      A wrapper script that calls other scripts. In this version calls 
      the SvnHotCopy.wsf
    - post-commit.bat
      To be placed in the repository hooks directory. This file is made 
      to call the script post-commit-wrapper.wsf.
    - readme.txt
      Info about the scripts above.
  - unfinished/
    - svnbck
      In a distant future it’s maybe going to be an all-round repository 
      backup program, but let’s first see if there are any other wheels 
      around that’s already invented.

$Id$
vim: set tw=72 ts=2 sw=2 sts=2 et fo+=w fenc=UTF-8 :
